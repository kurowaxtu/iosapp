//
//  LandmarksApp.swift
//  Landmarks
//
//  Created by 池田充宏 on 2020/11/04.
//

import SwiftUI

@main
struct LandmarksApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
