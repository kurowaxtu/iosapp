//
//  ContentView.swift
//  Landmarks
//
//  Created by 池田充宏 on 2020/11/04.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello")
            .padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
